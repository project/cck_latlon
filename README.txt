

CCK LatLon Creates a simple "Latitude/Longitude" storage for geographic 
coordinates.

Initial Features:
Simple storage of latitude/longitude pairs.
Simple views integration (listing, no filtering)

Future Features:
Integration with mapping modules to plot points
Stronger views support for filtering
Distance based searching
